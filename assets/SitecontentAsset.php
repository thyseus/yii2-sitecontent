<?php
namespace thyseus\sitecontent\assets;

use yii\web\AssetBundle;

/**
 * Class StoreAsset
 * @package app\assets
 */
class SitecontentAsset extends AssetBundle
{
    public $sourcePath = '@sitecontent/assets';

    public $css = [
        'css/sitecontent.css',
    ];

    public $js = [];
    public $depends = [
        'app\assets\AppAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG
    ];
}
