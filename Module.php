<?php

namespace thyseus\sitecontent;

use Yii;
use yii\i18n\PhpMessageSource;

/**
 * Sitecontent module definition class
 */
class Module extends \yii\base\Module
{
    public $version = '0.1.0-dev';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'thyseus\sitecontent\controllers';

    public $defaultRoute = 'sitecontent\sitecontent\index';

    /**
     * @var string The layout to be used by the view action. Keep null to use the default
     * layout of your application.
     */
    public $layout = null;

    /**
     * @var string The class of the User Model inside the application this module is attached to
     */
    public $userModelClass = 'app\models\User';

    /**
     * @var array|false Options for the Summernote WYSIWYG editor plugin. Set to false to disable the plugin.
     */
    public $summernoteOptions = [
        'clientOptions' => [],
    ];

    /**
     * @var callback Rule to determine which users are allowed to access the content management system.
     * Defaults to Yii::$app->user->can('admin')
     */
    public $accessCallback = null;

    /**
     * You can add additional sitecontent types of your application.
     * The labels of the standard ones provided by yii2-sitecontent are defined at:
     * @see models/sitecontent.php function getTypeLabels()
     *
     * @var array sitecontent types.
     */
    public $types = [
        'sitecontent' => 'sitecontent',
        'blogpost' => 'blogpost',
        'external' => 'external',
        'standard' => 'standard',
        'email' => 'email',
        'custom' => 'custom',
    ];

    /**
     * The index represents the type; the values represents the possible positions.
     *
     * @var array sitecontent positions.
     */
    public $positions = [
        'sitecontent' => [
            'default' => 'default',
        ],
        'blogpost' => [
            'default' => 'default',
        ],
        'external' => [
            'default' => 'default',
        ],
        'standard' => [
            'default' => 'default',
        ],
        'email' => [
            'default' => 'default',
        ],
        'custom' => [
            'default' => 'default',
        ],
    ];

    /** @var array The rules to be used in URL management. */
    public $urlRules = [
        'sitecontent/update/<language>/<id>' => 'sitecontent/sitecontent/update',
        'sitecontent/delete/<language>/<id>' => 'sitecontent/sitecontent/delete',
        'sitecontent/<language>/<id>' => 'sitecontent/sitecontent/view',
        'sitecontent/index' => 'sitecontent/sitecontent/index',
        'sitecontent/create/<type>' => 'sitecontent/sitecontent/create',
        'sitecontent/<id>' => 'sitecontent/sitecontent/view',
        'sitecontent/preview/<token>' => 'sitecontent/sitecontent/preview',
        'blog/<slug>' => 'sitecontent/sitecontent/blog',
        'blog' => 'sitecontent/sitecontent/blog',
        'category/index' => 'sitecontent/category/index',
        'category/create' => 'sitecontent/category/create',
        'tag/index' => 'sitecontent/tag/index',
        'tag/create' => 'sitecontent/tag/create',
        'tag/delete' => 'sitecontent/tag/delete'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->accessCallback) {
            $this->accessCallback = function () {
                return Yii::$app->user->can('admin');
            };
        }

        if (!isset(Yii::$app->get('i18n')->translations['sitecontent*'])) {
            Yii::$app->get('i18n')->translations['sitecontent*'] = [
                'class' => PhpMessageSource::className(),
                'basePath' => __DIR__ . '/messages',
                'sourceLanguage' => 'en-US'
            ];
        }


        $this->setAliases([
            '@sitecontent' => __DIR__,
        ]);

        parent::init();

    }
}
