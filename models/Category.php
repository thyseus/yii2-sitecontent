<?php

namespace thyseus\sitecontent\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "sitecontent_categories".
 *
 * @property string $id
 * @property string $parent
 * @property string $created_by
 * @property string $updated_by
 * @property string $language
 * @property integer $status
 * @property string $slug
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends ActiveRecord
{
    const STATUS_SYSTEM = -1;
    const STATUS_HIDDEN = 0;
    const STATUS_DRAFT = 1;
    const STATUS_PUBLIC = 2;
    const STATUS_RESTRICTED = 3;
    const STATUS_MOVED = 4;

    public static function tableName()
    {
        return 'sitecontent_categories';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'immutable' => true,
                'ensureUnique' => true,
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'name'], 'required'],
            [['status', 'parent'], 'integer'],
            [['description'], 'string'],
            [['language'], 'string', 'max' => 5],
            [['name', 'slug'], 'string', 'max' => 255],
            ['language', 'unique', 'targetAttribute' => ['id', 'language']],
            ['language', 'unique', 'targetAttribute' => ['slug', 'language']],
        ];
    }

    /**
     * @param string|null $category (optional) only get the sitecontent entries of the given category
     * @return \yii\db\ActiveQuery
     */
    public function getSitecontents(string $category = null)
    {
        $query = $this->hasMany(Sitecontent::class, ['id' => 'sitecontent_id', 'language' => 'sitecontent_language'])
                    ->viaTable('sitecontent_junction_categories', ['category_id' => 'id']);

        if ($category) {
            $query->andWhere(['category.slug' => $category]);
        }

        $query->andWhere(['status' => Sitecontent::STATUS_PUBLIC]);

        return $query;
    }

    public static function getStatusOptions()
    {
        return [
            Category::STATUS_SYSTEM => Yii::t('sitecontent', 'system'),
            Category::STATUS_HIDDEN => Yii::t('sitecontent', 'hidden'),
            Category::STATUS_DRAFT => Yii::t('sitecontent', 'draft'),
            Category::STATUS_PUBLIC => Yii::t('sitecontent', 'public'),
            Category::STATUS_RESTRICTED => Yii::t('sitecontent', 'resticted'),
            Category::STATUS_MOVED => Yii::t('sitecontent', 'moved'),
        ];
    }

    public function search($params)
    {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->filterWhere([
            'id' => $this->id,
            'parent' => $this->parent,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'language' => $this->language,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ;

        return $dataProvider;
    }

    /**
     * @return mixed the next free category id
     */
    public static function nextFreeId()
    {
        return Category::find()->orderBy('id DESC')->one()->id + 1;
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    public static function getParentsGrouped()
    {
        return Category::find()->where('parent is NULL')->all();
    }

    public function getParentModel()
    {
        return $this->hasOne(Category::class, ['id' => 'parent']);
    }

    public function getChilds()
    {
        return $this->hasMany(Category::className(), ['parent' => 'id']);
    }

    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->getModule('sitecontent')->userModelClass, ['id' => 'created_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->getModule('sitecontent')->userModelClass, ['id' => 'updated_by']);
    }
}