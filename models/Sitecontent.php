<?php

namespace thyseus\sitecontent\models;

use cornernote\linkall\LinkAllBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

/**
 * This is the model class for table "sitecontent".
 *
 * @property string  $id
 * @property string  $parent
 * @property string  $created_by
 * @property string  $updated_by
 * @property string  $language
 * @property string  $position
 * @property integer $status
 * @property string  $slug
 * @property string  $title
 * @property string  $content
 * @property string  $created_at
 * @property string  $updated_at
 * @property string  $views
 * @property string  $layout
 * @property string  $tags
 * @property string  $type
 */
class Sitecontent extends ActiveRecord
{
    const STATUS_SYSTEM = -1;
    const STATUS_HIDDEN = 0;
    const STATUS_DRAFT = 1;
    const STATUS_PUBLIC = 2;
    const STATUS_RESTRICTED = 3;
    const STATUS_MOVED = 4;

    public $category_ids = [];
    public $tag_ids = [];
    public $mode;

    const HTML_TARGETS = [
        '_blank',
        '_self',
        '_parent',
        '_top',
    ];

    const FORMS = [
        'sitecontent-store' => 'sitecontent',
        'sitecontent-projects' => 'sitecontent',
        'sitecontent-dashboard' => 'sitecontent',
        'blogpost-projects' => 'blogpost-projects',
        'external' => 'external',
        'email' => 'email',
        'file' => 'file',
    ];

    public static function getStatusOptions()
    {
        return [
            Sitecontent::STATUS_SYSTEM => Yii::t('sitecontent', 'system'),
            Sitecontent::STATUS_HIDDEN => Yii::t('sitecontent', 'hidden'),
            Sitecontent::STATUS_DRAFT => Yii::t('sitecontent', 'draft'),
            Sitecontent::STATUS_PUBLIC => Yii::t('sitecontent', 'public'),
            Sitecontent::STATUS_RESTRICTED => Yii::t('sitecontent', 'resticted'),
            Sitecontent::STATUS_MOVED => Yii::t('sitecontent', 'moved'),
        ];
    }

    /**
     * @return array map to i18n'ed version of types
     */
    public static function getTypeLabels()
    {
        $labels = [];

        foreach (Yii::$app->getModule('sitecontent')->types as $type) {
            $labels[$type] = Yii::t('sitecontent', $type);
        }

        return $labels;
    }

    public function getPositionLabel(): ?string
    {
        $types = Yii::$app->getModule('sitecontent')->positions;
        $positions = [];
        foreach ($types as $type => $s_position) {
            foreach ($s_position as $key => $position_label) {
                $positions[$key] = $position_label;
            }
        }

        return $positions[$this->position] ?? null;
    }

    /**
     * @param string|null $type
     *
     * @return array
     */
    public static function getPositionLabels(string $type = null)
    {
        $labels = [];

        if ($type) {
            $types[$type] = Yii::$app->getModule('sitecontent')->positions[$type];
        } else {
            $types = Yii::$app->getModule('sitecontent')->positions;
        }

        foreach ($types as $type => $positions) {
            if ($positions) {
                foreach ($positions as $key => $position) {
                    $labels[$type][$key] = Yii::t('sitecontent', $position);
                }
            }
        }

        return $labels;
    }

    /**
     * Get the correct view that corresponds to the type of this sitecontent.
     * Additional views can be overwritten for your project.
     *
     * @see thyseus/yii2-sitecontent/Module.php $types
     */
    public function getFormView()
    {
        if ($this->type) {
            $view_file = sprintf('form_%s', self::FORMS[$this->type]);

            if ($this->viewFileExists($view_file)) {
                return $view_file;
            }
        }
        return 'form_sitecontent'; # fallback
    }

    /**
     * @param string $view_file check that the given view file does exists.
     *                          It may be overridden in the application configuration via:
     *
     * @see https://www.yiiframework.com/doc/api/2.0/yii-base-theme#$pathMap-detail
     */
    protected function viewFileExists(string $view_file): bool
    {
        $base_path = Yii::$app->basePath;
        $path_module = $base_path . '/vendor/thyseus/yii2-sitecontent/views/sitecontent/' . $view_file . '.php';
        $path_app = $base_path . '/views/sitecontent/' . $view_file . '.php';

        return is_readable($path_module) || is_readable($path_app);
    }

    public static function getParentsGrouped()
    {
        return Sitecontent::find()
            ->where('parent is NULL')
            ->orderBy('title ASC')
            ->all();
    }

    public static function getLanguages()
    {
        $languages = [];

        foreach ((new \yii\db\Query())
                     ->select(['language'])
                     ->from(self::tableName())
                     ->groupBy('language')
                     ->indexBy('language')
                     ->all() as $lang) {
            $languages[$lang['language']] = $lang['language'];
        }

        return $languages;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sitecontent';
    }

    /**
     * @return mixed the next free sitecontent id
     */
    public static function nextFreeId()
    {
        return Sitecontent::find()->orderBy('id DESC')->one()->id + 1;
    }

    /* Retrieve raw content of an sitecontent entry, if available*/
    public static function getContent($id, $language = null)
    {
        $model = Sitecontent::findOne(['slug' => $id, 'language' => $language ? $language : Yii::$app->language]);

        if (!$model) {
            $model = Sitecontent::findOne(['id' => $id, 'language' => $language ? $language : Yii::$app->language]);
        }

        if (!$model) {
            $model = Sitecontent::findOne(['slug' => $id]);
        }

        if (!$model) {
            $model = Sitecontent::findOne(['id' => $id]);
        }

        if ($model) {
            return $model->content;
        } else {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => true,
            ],
            'previewToken' => [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'preview_token',
                'value' => function () {
                    return substr(md5(uniqid()), 0, 50);
                },
                'immutable' => true,
                'ensureUnique' => true,
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => LinkAllBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'title'], 'required'],
            [['tag_ids', 'category_ids'], 'each', 'rule' => ['string']],
            [['status', 'views', 'parent', 'target'], 'integer'],
            [['content', 'meta_title', 'meta_description', 'meta_keywords', 'mode'], 'string'],
            ['content', 'required', 'when' => function ($model) {
                return $model->type == "external";
            }, 'whenClient' => "function (attribute, value) {return $('#sitecontent-type').val() == 'external'; }"],
            ['content', 'url', 'when' => function ($model) {
                return $model->type == "external";
            }, 'whenClient' => "function (attribute, value) {return $('#sitecontent-type').val() == 'external'; }"],
            [['language'], 'string', 'max' => 5],
            [['title', 'slug', 'layout', 'type', 'position'], 'string', 'max' => 255],
            ['language', 'unique', 'targetAttribute' => ['id', 'language']],
            ['language', 'unique', 'targetAttribute' => ['slug', 'language']],
        ];
    }

    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])
            ->viaTable('sitecontent_junction_categories',
                ['sitecontent_id' => 'id', 'sitecontent_language' => 'language']);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])
            ->viaTable('sitecontent_junction_tags',
                ['sitecontent_id' => 'id', 'sitecontent_language' => 'language']);
    }

    public function beforeValidate()
    {
        if ($this->id == $this->parent) {
            $this->addError('parent', Yii::t('sitecontent', 'Parent id can not be identical to the sitecontent id'));
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     *
     * @see http://htmlpurifier.org/phorum/read.php?3,7023
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord && !$this->meta_title) {
            $this->meta_title = substr($this->title, 0, 70); // recommended max length
        }

        $this->content = HtmlPurifier::process($this->content, function ($config) {
            $config->getHTMLDefinition(true)
                ->addAttribute('a', 'target', 'Text');
        });

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        // link tags
        $tags = [];

        foreach ($this->tag_ids as $name) {
            // if tag does not exist then create
            array_push($tags, Tag::find()->where(['name' => $name])->one() ?? Tag::of($name, $this->language));
        }

        $this->linkAll('tags', $tags);

        // link categories
        $categories = [];
        if ($this->category_ids) {
            $categories = Category::find()->where(['in', 'id', $this->category_ids])->all();
        }
        $this->linkAll('categories', $categories);

        if (!$insert) {
            // create redirection 301 from the old to the new url
            $oldSlug = $changedAttributes['slug'] ?? $this->slug;

            Redirection::create(
                Url::to(['//sitecontent/sitecontent/view', 'id' => $oldSlug], true),
                Url::to(['//sitecontent/sitecontent/view', 'id' => $this->slug], true),
                301
            );

            if ($this->status == Sitecontent::STATUS_PUBLIC) {
                $this->updateAttributes(['preview_token' => null]);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('sitecontent', '#'),
            'parent' => Yii::t('sitecontent', 'parent'),
            'created_by' => Yii::t('sitecontent', 'created by'),
            'updated_by' => Yii::t('sitecontent', 'last updated by'),
            'language' => Yii::t('sitecontent', 'language'),
            'position' => Yii::t('sitecontent', 'position'),
            'status' => Yii::t('sitecontent', 'status'),
            'slug' => Yii::t('sitecontent', 'slug'),
            'title' => Yii::t('sitecontent', 'title'),
            'content' => Yii::t('sitecontent', 'content'),
            'created_at' => Yii::t('sitecontent', 'created at'),
            'updated_at' => Yii::t('sitecontent', 'last updated at'),
            'views' => Yii::t('sitecontent', 'views'),
            'layout' => Yii::t('sitecontent', 'layout'),
            'tags' => Yii::t('sitecontent', 'tags'),
            'type' => Yii::t('sitecontent', 'type'),
            'meta_title' => Yii::t('sitecontent', 'meta title'),
            'meta_description' => Yii::t('sitecontent', 'meta description'),
            'meta_keywords' => Yii::t('sitecontent', 'meta keywords'),
        ];
    }

    public function getParentSitecontent()
    {
        return $this->hasOne(Sitecontent::className(), ['id' => 'parent']);
    }

    public function getChilds()
    {
        return $this->hasMany(Sitecontent::className(), ['parent' => 'id']);
    }

    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->getModule('sitecontent')->userModelClass, ['id' => 'created_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->getModule('sitecontent')->userModelClass, ['id' => 'updated_by']);
    }
}
