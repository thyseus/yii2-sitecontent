<?php

namespace thyseus\sitecontent\models;

use cornernote\linkall\LinkAllBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "sitecontent_tags".
 *
 * @property string $id
 * @property string $created_by
 * @property string $updated_by
 * @property string $language
 * @property integer $status
 * @property string $slug
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Tag extends ActiveRecord
{
    const STATUS_SYSTEM = -1;
    const STATUS_HIDDEN = 0;
    const STATUS_DRAFT = 1;
    const STATUS_PUBLIC = 2;
    const STATUS_RESTRICTED = 3;
    const STATUS_MOVED = 4;

    public static function tableName()
    {
        return 'sitecontent_tags';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'immutable' => true,
                'ensureUnique' => true,
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => LinkAllBehavior::className(),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'name'], 'required'],
            [['status'], 'integer'],
            [['language'], 'string', 'max' => 5],
            [['name', 'slug'], 'string', 'max' => 255],
            ['language', 'unique', 'targetAttribute' => ['id', 'language']],
            ['language', 'unique', 'targetAttribute' => ['slug', 'language']],
        ];
    }

    /**
     * @param string $name
     * @return Tag
     */
    public static function of(string $name, string $language)
    {
        $tag = new self();

        $tag->name = $name;
        $tag->language = $language;
        $tag->status = self::STATUS_PUBLIC;

        $tag->save();

        return $tag;
    }

    public function getSitecontents()
    {
        return $this->hasMany(Sitecontent::class, [
            'id' => 'sitecontent_id',
            'language' => 'sitecontent_language',
        ])->viaTable('sitecontent_junction_tags', ['tag_id' => 'id']);
    }

    public static function getStatusOptions()
    {
        return [
            Tag::STATUS_SYSTEM => Yii::t('sitecontent', 'system'),
            Tag::STATUS_HIDDEN => Yii::t('sitecontent', 'hidden'),
            Tag::STATUS_DRAFT => Yii::t('sitecontent', 'draft'),
            Tag::STATUS_PUBLIC => Yii::t('sitecontent', 'public'),
            Tag::STATUS_RESTRICTED => Yii::t('sitecontent', 'resticted'),
            Tag::STATUS_MOVED => Yii::t('sitecontent', 'moved'),
        ];
    }

    public function search($params)
    {
        $query = Tag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->filterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'language' => $this->language,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'name', $this->name])
        ;

        return $dataProvider;
    }

    /**
     * @return mixed the next free tag id
     */
    public static function nextFreeId()
    {
        return Tag::find()->orderBy('id DESC')->one()->id + 1;
    }

    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->getModule('sitecontent')->userModelClass, ['id' => 'created_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->getModule('sitecontent')->userModelClass, ['id' => 'updated_by']);
    }
}