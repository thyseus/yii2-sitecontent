<?php

namespace thyseus\sitecontent\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "sc_redirection".
 *
 * @property string $id
 * @property string $origin
 * @property string $target
 * @property string $code
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Redirection extends ActiveRecord
{
    public static function tableName()
    {
        return 'sitecontent_redirection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origin', 'target', 'code'], 'required'],
            [['origin', 'target', 'code'], 'string']
        ];
    }

    /**
     * When the title of a sitecontent changes, we also change the slug
     * of the sitecontent. The old URL should still be valid and be
     * redirected to the url of the new sitecontent.
     *
     * @param $origin
     * @param $target
     * @param string $code 301 (permanent) or 302 (temporary)
     * @return null|Redirection
     */
    public static function create(string $origin, string $target, int $code = 301)
    {
        if ($code != 301 && $code != 302) {
            throw new \Exception('Please choose either 301 (permanent) or 302 (temporary) redirection');
        }

        $redirection = Redirection::findOne(['origin' => $origin]);

        if ($target === $origin) {
            // avoiding duplicate redirection
            return ($redirection) ? $redirection->delete() : null;
        }

        if (!$redirection) {
            $redirection = new self();
            $redirection->origin = $origin;
        }

        $redirection->target = $target;
        $redirection->code = (string) $code;

        $redirection->save();

        return $redirection;
    }
}