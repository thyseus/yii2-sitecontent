<?php

namespace thyseus\sitecontent\services;

use thyseus\sitecontent\models\Sitecontent;
use yii\helpers\Html;

class MenuService
{
    /**
     * Retrieve a <ul> list of all menu entries under $position
     * @param string $position
     */
    public function generateMenu(string $position)
    {
        $entries = Sitecontent::find()->where([
            'status' => Sitecontent::STATUS_PUBLIC,
            'position' => $position,
        ])->all();

        $string = '<ul>';

        foreach ($entries as $entry) {
            $string .= '<li>';
            $string .= Html::a($entry->title, ['//sitecontent/sitecontent/view', 'id' => $entry->slug]);
            $string .= '</li>';
        }

        $string .= '</ul>';

        return $string;

    }

}