<?php

namespace thyseus\sitecontent\services;

use thyseus\sitecontent\models\Category;
use thyseus\sitecontent\models\Sitecontent;
use thyseus\sitecontent\models\Tag;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use Yii;

class SitecontentService
{
    public function getPositions(): array
    {
        $return_positions = [];
        foreach (Yii::$app->getModule('sitecontent')->positions as $type => $positions) {

            foreach ($positions as $position) {
                $return_positions[] = $position;
            }
        }

        return $return_positions;
    }

    public function getPositionLabels()
    {
        $items = [];
        foreach ($this->getPositions() as $position) {
            $items[] = [
                'label' => Yii::t('sitecontent', $position),
                'url' => Url::to(['sitecontent/create/', 'position' => $position]
                )
            ];
        }

        return $items;
    }

    public function determineType(string $position = null): string
    {
        foreach (Yii::$app->getModule('sitecontent')->positions as $type => $positions) {
            foreach ($positions as $key_position => $i_position) {
                if ($position == $key_position) {
                    return $type;
                }
            }
        }

        return 'sitecontent';
    }

    public function getBlogPosts(string $category = null, $tag = null)
    {
        $query = Sitecontent::find()->where([
            'type' => 'blogpost',
            'sitecontent.status' => Category::STATUS_PUBLIC
        ]);

        if ($category) {
            $query->joinWith('categories');
                $query->andWhere(['sitecontent_categories.slug' => $category]);
        }

        if ($tag) {
            $query->joinWith('tags');
            $query->andWhere(['sitecontent_tags.slug' => $tag]);
        }

        return $query->all();
    }

    public function getTags()
    {
        return Tag::find()->where(['status' => Tag::STATUS_PUBLIC])->all();
    }

    public function getCategories(string $parent_category = null)
    {
        $query = Category::find()->where([
            'status' => Category::STATUS_PUBLIC
        ]);

        if ($parent_category) {
            $parentCategory = Category::find()->where(['slug' => $parent_category])->one();
            if ($parentCategory) {
                $query->andWhere(['parent' => $parentCategory->id]);
            }
        }

        $query->andWhere('parent IS NOT NULL');

        return $query->all();
    }

    /**
     * Return all possible selectableparent elements the given Sitecontent Model
     * @param Sitecontent $model
     * @return array
     */
    public function getParents(Sitecontent $model)
    {
        $result = Sitecontent::find();

        if (!$model->isNewRecord) {
            $result->where(['!=', 'id', $model->id]);
        }

        $result->andWhere(['position' => $model->position]);
        $result->orderBy('title ASC');

        $result = $result->all();

        return ArrayHelper::map($result, 'id', 'title');
    }

}
