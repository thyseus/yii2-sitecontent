<?php

namespace thyseus\sitecontent\controllers;

use thyseus\sitecontent\models\Tag;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

/**
 * Class TagController
 * @package thyseus\sitecontent\controllers
 */
class TagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'matchCallback' => Yii::$app->getModule('sitecontent')->accessCallback,
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sitecontent models.
     * @var $tree set to true to display an Tree View. Filtering is disabled then, though :-(
     * @return mixed
     */
    public function actionIndex($tree = false)
    {
        $searchModel = new Tag();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',
            [
                'tree' => null,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionView($id)
    {

    }

    public function actionCreate($source_id = null, $source_language = null, $target_language = null)
    {
        $model = new Tag();

        if ($source_id && $source_language) {
            if ($source = Tag::find()->where(['id' => $source_id, 'language' => $source_language])->one()) {
                $model->attributes = $source->attributes;
            } else {
                throw new NotFoundHttpException('The source sitecontent could not be found');
            }

            if ($target_language) {
                $model->language = $target_language;
            }
        }

        $model->status = 0;

        if (!$model->language && isset(Yii::$app->language))
            $model->language = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('form', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        if (!$model = Tag::findOne(['id' => $id])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('form', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        Tag::findOne($id)->delete();

        return $this->redirect(['index']);
    }
}