<?php

namespace thyseus\sitecontent\controllers;

use Symfony\Component\VarDumper\VarDumper;
use thyseus\sitecontent\models\Category;
use thyseus\sitecontent\models\Redirection;
use thyseus\sitecontent\models\Sitecontent;
use thyseus\sitecontent\models\SitecontentSearch;
use thyseus\sitecontent\models\Tag;
use thyseus\sitecontent\services\SitecontentService;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SitecontentController implements the CRUD actions for Sitecontent model.
 */
class SitecontentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'matchCallback' => Yii::$app->getModule('sitecontent')->accessCallback,
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view', 'blog'],
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sitecontent models.
     * @var $tree set to true to display an Tree View. Filtering is disabled then, though :-(
     * @return mixed
     */
    public function actionIndex($tree = false)
    {
        $searchModel = new SitecontentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'tree' => $tree,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sitecontent model.
     * If the sitecontent is available in another language than requested, a redirection to the correct page is
     * being returned.
     * @param string $id
     * @param string $language
     * @return mixed
     */
    public function actionView($id, $language = null, $preview_token = null)
    {
        if (!$language) {
            $language = Yii::$app->language;
        }

        if ($redirection = $this->isRedirect($id)) {
            return $this->redirect($redirection->target, $redirection->code);
        }

        $model = $this->findModel($id, $language);

        // apply the layout of the activerecord model, of the module or the application (in order)
        if ($model->layout) {
            $this->layout = $model->layout;
        } else {
            $layout = Yii::$app->getModule('sitecontent')->layout;
            if ($layout) {
                $this->layout = $layout;
            }
        }

        if ($target = $this->redirectionNecessary($model, $id, $language)) {
            return $this->redirect($target, 301);
        }

        if ($model->status != Sitecontent::STATUS_PUBLIC
            && ($model->preview_token !== null && $model->preview_token !== $preview_token)
        ) {
            throw new NotFoundHttpException();
        }

        $model->updateCounters(['views' => 1]);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function isRedirect($id)
    {
        $origin = Url::to(['//sitecontent/sitecontent/view', 'id' => $id], true);

        return Redirection::findOne(['origin' => $origin]);
    }

    /**
     * Finds the Sitecontent model.
     * May fall back to a not requested language if slug or id is equivalent.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $language
     * @return Sitecontent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $language = null)
    {
        if (!$language) {
            $language = Yii::$app->language;
        }

        if (($model = Sitecontent::findOne(['slug' => $id, 'language' => $language])) !== null) {
            return $model;
        } else if (($model = Sitecontent::findOne(['id' => $id, 'language' => $language])) !== null) {
            return $model;
        } else if (($model = Sitecontent::findOne(['id' => $id])) !== null) {
            return $model;
        } else if (($model = Sitecontent::findOne(['slug' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Check if an redirection is necessary for the given sitecontent. If so, return the route to the correct target.
     * @param $model model to check against
     * @param $id id to check against
     * @param $language language to check against
     * @return array
     */
    protected function redirectionNecessary($model, $id, $language)
    {
        if ($model->language != $language && $correct_model = Sitecontent::findOne(['id' => $model->id, 'language' => $language])) {
            return ['//sitecontent/sitecontent/view', 'id' => $correct_model->slug];
        }
    }

    /**
     * Creates a new Sitecontent model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @param null $source_id give an id to copy the content as a template for thew new Record
     * @param null $source_language the language of the source
     * @param null $target_language the target language
     * @param null $position the position where the new content should appear
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate(
        $source_id = null,
        $source_language = null,
        $target_language = null,
        $content_type = null,
        $position = null)
    {
        $service = new SitecontentService;
        $model = new Sitecontent;

        $model->views = 0;
        $model->status = 0;
        $model->position = $position;
        $model->type = $content_type; //$service->determineType($position);

        if ($source_id && $source_language) {
            if ($source = Sitecontent::find()->where(['id' => $source_id, 'language' => $source_language])->one()) {
                $model->attributes = $source->attributes;
            } else {
                throw new NotFoundHttpException('The source sitecontent could not be found');
            }

            if ($target_language) {
                $model->language = $target_language;
            }
        }

        if (!$model->type || !in_array($model->type, Yii::$app->getModule('sitecontent')->types)) {
            throw new \InvalidArgumentException('invalid type param');
        }

        if (!$model->id) {
            $model->id = Sitecontent::nextFreeId();
        }

        if (!$model->language && isset(Yii::$app->language))
            $model->language = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success',
                Yii::t('sitecontent', 'Your sitecontent has been created'));
            return $this->redirect(['update', 'id' => $model->slug]);
        } else {
            return $this->render($model->formView, [
                'model' => $model,
            ]);
        }

    }

    /**
     * Updates an existing Sitecontent model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $language
     * @return mixed
     */
    public function actionUpdate($id, $language = null)
    {
        $model = $this->findModel($id, $language);

        Yii::$app->user->setReturnUrl(['//sitecontent/sitecontent/update', 'id' => $model->id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            // assign selected values
            $model->tag_ids = ArrayHelper::map($model->tags, 'name', 'name');
            $model->category_ids = ArrayHelper::map($model->categories, 'id', 'id');

            return $this->render($model->formView, [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sitecontent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $language
     * @return mixed
     */
    public function actionDelete($id, $language)
    {
        $this->findModel($id, $language)->delete();

        return $this->redirect(['index']);
    }

    public function actionBlog(string $category = null, $tag = null)
    {
        $service = new SitecontentService;

        $blogposts = $service->getBlogPosts($category, $tag);
        $tags = $service->getTags();
        $categories = $service->getCategories($category);

        return $this->render('blog', [
            'blogposts' => $blogposts,
            'tags' => $tags,
            'categories' => $categories,
        ]);
    }

}
