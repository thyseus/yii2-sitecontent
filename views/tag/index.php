<?php

use thyseus\sitecontent\models\Tag;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SitecontentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('sitecontent', 'Tag');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sitecontent-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            Yii::t('sitecontent', 'Add Tag'),
            ['create'],
            ['class' => 'btn btn-primary']
        ); ?>
    </p>

    <?php $widget_options = [
        'dataProvider' => $dataProvider,
        'filterModel' => $tree ? false : $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width:50px;'],
            ],
            [
                'format' => 'raw',
                'attribute' => 'name',
                'value' => function ($data) {
                    return Html::a($data->name, ['update', 'id' => $data->slug, 'language' => $data->language], ['data-pjax' => 0]);
                },
            ],
            [
                'attribute' => 'language',
                'filter' => ['en', 'de'],
            ],
            [
                'attribute' => 'status',
                'filter' => Tag::getStatusOptions(),
                'value' => function ($model) {
                    return Tag::getStatusOptions()[$model->status];
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{copy}{delete}',
                'buttons' => [
                    'copy' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-copy"></span>', [
                            '//sitecontent/tag/create', 'source_id' => $model->id, 'source_language' => $model->language],
                            ['title' => Yii::t('sitecontent', 'Copy'), 'data-pjax' => 0]);
                    },

                ],
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return $model->status === Tag::STATUS_PUBLIC;
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(['tag/' . $action, 'id' => $model->id]);
                }
            ],
        ]
    ];

    echo GridView::widget($widget_options);
    ?>
</div>
