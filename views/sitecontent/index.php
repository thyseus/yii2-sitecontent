<?php

use thyseus\sitecontent\models\Sitecontent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use thyseus\sitecontent\services\SitecontentService;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SitecontentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('sitecontent', 'Sitecontent');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sitecontent-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

        <?= $this->render('_header_index', ['tree' => $tree]); ?>

        <?php $widget_options = [
            'dataProvider' => $dataProvider,
            'filterModel' => $tree ? false : $searchModel,
            'columns' => [
                [
                    'attribute' => 'id',
                    'headerOptions' => ['style' => 'width:50px;'],
                ],
                [
                    'attribute' => 'position',
                    'filter' => Yii::$app->getModule('sitecontent')->positions,
                    'value' => function ($model) {
                        return $model->positionLabel;
                    },
                ],
                [
                    'attribute' => 'parent',
                    'value' => function ($model) {
                        return $model->parentSitecontent ? $model->parentSitecontent->title : null;
                    },
                    'filter' => ArrayHelper::map(Sitecontent::getParentsGrouped(), 'id', 'title'),
                ],
                [
                    'format' => 'raw',
                    'attribute' => 'title',
                    'value' => function ($data) {
                        return Html::a($data->title, ['update', 'id' => $data->slug, 'language' => $data->language],
                            ['data-pjax' => 0]);
                    },
                ],
                [
                    'attribute' => 'slug',
                ],
                [
                    'attribute' => 'language',
                    'filter' => Sitecontent::getLanguages(),
                ],
                [
                    'attribute' => 'status',
                    'filter' => Sitecontent::getStatusOptions(),
                    'value' => function ($model) {
                        return Sitecontent::getStatusOptions()[$model->status];
                    },
                ],
                'views',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}{copy}{delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [
                                yii\helpers\Url::to('/inhalt/' . $model->id),
                            ],
                                ['title' => Yii::t('sitecontent', 'View'), 'data-pjax' => 0]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [
                                yii\helpers\Url::to('/inhalt/bearbeiten/' . $model->language . '/' . $model->id),
                            ],
                                ['title' => Yii::t('sitecontent', 'Update'), 'data-pjax' => 0]);
                        },
                        'copy' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-copy"></span>', [
                                '//sitecontent/sitecontent/create',
                                'source_id' => $model->id,
                                'source_language' => $model->language,
                            ],
                                ['title' => Yii::t('sitecontent', 'Copy'), 'data-pjax' => 0]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                [
                                    yii\helpers\Url::to('/inhalt/loeschen/' . $model->language . '/' . $model->id),
                                ],
                                [
                                    'title' => Yii::t('sitecontent', 'Delete'),
                                    'data-pjax' => 0,
                                    'data-method' => 'POST',
                                    'data-confirm' => 'Sind Sie sich sicher, dass Sie dieses Element entfernen möchten?',
                                ]
                            );
                        },
                    ],
                    'visibleButtons' => [
                        'view' => function ($model, $key, $index) {
                            return ($model->status === Sitecontent::STATUS_PUBLIC && $model->type !== 'external');
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return Url::to([
                            'sitecontent/' . $action,
                            'id' => $model->slug,
                            'language' => $model->language,
                        ]);
                    },
                ],
            ],
        ];

        if ($tree) {
            $widget_options['parentColumnName'] = 'parent';
        }

        echo \yii\grid\GridView::widget($widget_options);
        ?>
</div>
