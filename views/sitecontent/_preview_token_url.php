<?php use yii\helpers\Url; ?>
<?php use yii\helpers\Html; ?>

<?php if (!$model->isNewRecord) { ?>
    <?php if ($model->preview_token) { ?>
        <?php $url = Url::to(['view',
            'id' => $model->slug,
            'preview_token' => $model->preview_token,
        ], true); ?>
        <?= Yii::t('sitecontent',
            'The preview url is: <strong> {preview_token_url} </strong>', [
                'preview_token_url' => Html::a($url, $url, ['target' => '_blank'])]); ?>
    <?php } else { ?>
        <?= Yii::t('sitecontent', 'No preview token url available.'); ?>
    <?php } ?>
<?php } ?>
