<?php

use yii\helpers\Html;

$this->title = 'blog';
$this->params['breadcrumbs'][] = ['label' => Yii::t('sitecontent', 'Sitecontent'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('sitecontent', 'blog');
?>
<div class="col-md-3 p0">
    <div class="panel panel-default">
        <div class="panel-heading">
            <?= Yii::t('sitecontent', 'Categories') ?>
        </div>
        <div class="panel-body">
            <?php foreach ($categories as $category) { ?>
                <li><?= Html::a(
                        $category->name,
                        ['sitecontent/blog', 'category' => $category->slug]
                    ); ?></li>
            <?php } ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <?= Yii::t('sitecontent', 'Tags') ?>
        </div>
        <div class="panel-body">
            <?php foreach ($tags as $tag) { ?>
                <?= Html::a(
                        $tag->name,
                        ['sitecontent/blog', 'tag' => $tag->slug]
                    ); ?>,
            <?php } ?>
        </div>
    </div>

</div>
<div class="col-md-9">
    <?php foreach ($blogposts as $blog) { ?>
        <div>
            <h3><?= Html::a(
                $blog->title,
                ['sitecontent/view', 'id' => $blog->slug]
            ); ?>
            </h3>
            <p><?= \yii\helpers\StringHelper::truncate($blog->content, 500) ?>
            <p><?= Html::a(
                Yii::t('sitecontent', 'Read more...'),
                ['sitecontent/view', 'id' => $blog->slug]
            ); ?>
            </p>
        </div>
    <?php } ?>
</div>
