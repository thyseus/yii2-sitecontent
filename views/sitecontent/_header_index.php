<?php

use thyseus\sitecontent\services\SitecontentService;
use yii\helpers\Html;

?>
<?= Html::a(
    Yii::t('sitecontent', 'Manage Categories'),
    ['category/index'],
    ['class' => 'btn btn-primary pull-left mr-10']
); ?>

<?= Html::a(
    Yii::t('sitecontent', 'Manage Tags'),
    ['tag/index'],
    ['class' => 'btn btn-primary pull-left mr-10']
); ?>
</p>
<div class="dropdown btn-group">
    <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">
        <?= Yii::t('sitecontent', 'Add sitecontent') ?>
    </a>
    <?php
    $service = new SitecontentService;
    echo \yii\bootstrap\Dropdown::widget([
        'items' => $service->getPositionLabels(),
    ]);
    ?>
</div>
