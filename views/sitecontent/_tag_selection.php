<?php
use yii\helpers\ArrayHelper;
use thyseus\sitecontent\models\Tag;
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= Yii::t('sitecontent', 'Tags') ?>
    </div>
    <div class="panel-body">
        <?= $form->field($model, 'tag_ids')->label(false)->checkboxList(
            ArrayHelper::map(Tag::find()->all(), 'name', 'name')
        ) ?>
    </div>
</div>
