<?php use thyseus\sitecontent\services\SitecontentService; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= Yii::t('sitecontent', 'Parent element') ?>
    </div>
    <div class="panel-body">
        <?php $service = new SitecontentService;

        $parents = $service->getParents($model);

        $parents = [null => Yii::t('sitecontent', 'None')] + $parents;

        echo $form->field($model, 'parent')->radioList(
            $parents, [
            'separator' => '<br>',
        ])->label(false); ?>
    </div>
