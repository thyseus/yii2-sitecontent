<?php

use yii\helpers\ArrayHelper;
use thyseus\sitecontent\models\Category;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= Yii::t('sitecontent', 'Categories') ?>
    </div>
    <div class="panel-body">
        <?= $form->field($model, 'category_ids')->label(false)->checkboxList(
            ArrayHelper::map(
                Category::find()
                    ->orderBy('title ASC')
                    ->all(), 'id', 'name')
        ) ?>
    </div>
</div>
