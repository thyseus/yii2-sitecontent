<?php use thyseus\sitecontent\models\Sitecontent; ?>

<div class="col-md-6 p0">
    <?= $form->field($model, 'position')->dropDownList(Sitecontent::getPositionLabels($model->type)) ?>
</div>

<div class="col-md-6 pl25 pr0">
    <?= $form->field($model, 'type')->dropDownList(Sitecontent::getTypeLabels(), [
        'options' => [Yii::$app->request->get('type') => ['selected' => true]],
        'disabled' => true,
    ]); ?>
</div>
