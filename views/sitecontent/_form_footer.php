<?php
use yii\helpers\Html;
use thyseus\sitecontent\models\Sitecontent;
?>

<div class="form-group">
    <?= Html::a(Yii::t('sitecontent', 'Back to Index'), ['index'], ['class' => 'btn btn-default']); ?>

    <?php if (!$model->isNewRecord) { ?>
        <?= Html::a(
            Yii::t('sitecontent', 'Copy entry'),
            ['create', 'source_id' => $model->id, 'source_language' => $model->language],
            ['class' => 'btn btn-default btn-copy-sitecontent']
        ); ?>

        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-select-language"
                    data-toggle="dropdown">
                <?= Yii::t('sitecontent', 'Language'); ?>: <?= $model->language; ?>
                <span class="caret"></span>
            </button>

            <ul class="dropdown-menu">
                <?php foreach (Sitecontent::getLanguages() as $language) {

                    if ($language != $model->language) {
                        if (Sitecontent::find()->where([
                            'slug' => $model->slug,
                            'language' => $language,
                        ])->exists()) { ?>
                            <li>
                                <?= Html::a(
                                    Yii::t('sitecontent', 'Switch to this page in language ') . $language,
                                    ['update', 'id' => $model->slug, 'language' => $language]); ?>
                            </li>
                        <?php } else { ?>
                            <li>
                                <?= Html::a(
                                    Yii::t('sitecontent', 'Copy this page in language ') . $language,
                                    ['create', 'source_id' => $model->id, 'source_language' => $model->language, 'target_language' => $language]); ?>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>

    <?= Html::submitButton(Yii::t('sitecontent', 'save'),
        ['class' => 'btn btn-success pull-right']) ?>

    <?= $this->render('_preview_token_url', [
        'model' => $model,
    ]); ?>
</div>
