<?php

use marqu3s\summernote\Summernote;
use thyseus\sitecontent\models\Sitecontent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sitecontent */

$this->title = $model->title;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('sitecontent', 'Sitecontent'),
    'url' => ['index'],
];
if ($model->status === Sitecontent::STATUS_PUBLIC) {
    $this->params['breadcrumbs'][] = [
        'label' => $model->title,
        'url' => ['view', 'id' => $model->slug, 'language' => $model->language],
    ];
}
$this->params['breadcrumbs'][] = Yii::t('sitecontent',
    $model->isNewRecord ? 'Create Externen Link' : 'Update Externen Link');
?>
    <div class="sitecontent-update">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="sitecontent-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-9">
                    <?= $this->render('_form_header', ['form' => $form, 'model' => $model]); ?>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'content')->textInput(['maxlength' => true]) ?>

                    <div class="col-md-6 p0">
                        <?= $form->field($model, 'target')->label(Yii::t('sitecontent', 'target'))->dropDownList(Sitecontent::HTML_TARGETS); ?>
                    </div>

                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'status')->dropDownList(Sitecontent::getStatusOptions()); ?>

                    <?= $form->field($model, 'language')->textInput(['maxlength' => true]) ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?= Yii::t('sitecontent', 'Info') ?>
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'created_by')->textInput(['disabled' => true]) ?>

                            <?= $form->field($model, 'updated_by')->textInput(['disabled' => true]) ?>

                            <?= $form->field($model, 'created_at')->textInput(['disabled' => true]) ?>

                            <?= $form->field($model, 'updated_at')->textInput(['disabled' => true]) ?>
                        </div>
                    </div>

                    <?= $this->render('_parent', ['form' => $form, 'model' => $model]); ?>
                </div>
            </div>

            <?= $this->render('_form_footer', ['form' => $form, 'model' => $model]); ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

<?php $this->registerJS(<<<JS
    $('input, textarea').change(function()
    {
        $('.btn-copy-sitecontent').attr('disabled', 'disabled');  
        $('.btn-copy-sitecontent').addClass('disabled');  
        
        $('.btn-select-language').attr('disabled', 'disabled');  
        $('.btn-select-language').addClass('disabled');  
    });
JS
);
