<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * @author Herbert Maschke <thyseus@gmail.com
 */
class m161109_084412_init_sitecontent extends Migration
{
    public function up()
    {
        $tableOptions = '';

        if (Yii::$app->db->driverName == 'mysql')
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%sitecontent}}', [
            'id' => Schema::TYPE_PK,
            'parent' => Schema::TYPE_INTEGER,
            'created_by' => Schema::TYPE_INTEGER,
            'updated_by' => Schema::TYPE_INTEGER,
            'language' => Schema::TYPE_STRING . '(5)',
            'position' => $this->string(255)->null(),
            'status' => Schema::TYPE_INTEGER,
            'slug' => Schema::TYPE_STRING . '(255) NOT NULL',
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
            'views' => Schema::TYPE_INTEGER,
            'meta_title' => $this->string(),
            'meta_description' => $this->string(),
            'meta_keywords' => $this->string(),
            'layout' => $this->string(255),
            'type' => $this->string(255),
            'preview_token' => $this->string()->null(),
            'target' => $this->integer(),
        ], $tableOptions);

        $this->insert('{{%sitecontent}}', [
            'id' => 1,
            'parent' => null,
            'title' => 'Root Node',
            'slug' => 'root-node',
            'status' => -1,
            'views' => 0,
            'language' => 'en-US',
            'created_at' => date('Y-m-d G:i:s'),
        ]);

        $this->alterColumn('{{%sitecontent}}', 'id', 'int(11) UNSIGNED NOT NULL');

        $this->dropPrimaryKey('id', '{{%sitecontent}}');

        $this->addPrimaryKey('id', '{{%sitecontent}}', ['id', 'language']);

        $this->createTable('{{%sitecontent_redirection}}', [
            'id' => $this->primaryKey(),
            'origin' => $this->text(),
            'target' => $this->text(),
            'code' => $this->string(5),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->createTable('{{%sitecontent_categories}}', [
            'id' => $this->primaryKey(),
            'parent' => $this->integer(),
            'name' => $this->string(255)->notNull(),
            'slug' => $this->string(255)->notNull(),
            'language' => $this->string(5),
            'description' => $this->text(),
            'status' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%sitecontent_junction_categories}}', [
            'sitecontent_id' => 'int(11) UNSIGNED NOT NULL',
            'sitecontent_language' => $this->string(5),
            'category_id' => $this->integer(),
            'PRIMARY KEY(sitecontent_id, sitecontent_language, category_id)',
        ], $tableOptions);

        // creates index for column `sitecontent_id`
        $this->createIndex(
            'idx-sitecontent_categories-sitecontent_id',
            '{{%sitecontent_junction_categories}}',
            'sitecontent_id'
        );

        // add foreign key for table `sitecontent`
        $this->addForeignKey(
            'fk-sitecontent_categories-sitecontent_id',
            '{{%sitecontent_junction_categories}}',
            'sitecontent_id, sitecontent_language',
            'sitecontent',
            'id, language',
            'CASCADE'
        );

        // creates index for column `sc_category_id`
        $this->createIndex(
            'idx-sitecontent_categories-category_id',
            '{{%sitecontent_junction_categories}}',
            'category_id'
        );

        // add foreign key for table `sitecontent_junction_category`
        $this->addForeignKey(
            'fk-sitecontent_categories-category_id',
            '{{%sitecontent_junction_categories}}',
            'category_id',
            'sitecontent_categories',
            'id',
            'CASCADE'
        );

        $this->createTable(' {{%sitecontent_tags}}', [
            'id' => $this->primaryKey(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'language' => $this->string(5),
            'status' => $this->integer(),
            'slug' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%sitecontent_junction_tags}}', [
            'sitecontent_id' => 'int(11) UNSIGNED NOT NULL',
            'sitecontent_language' => $this->string(5),
            'tag_id' => $this->integer(),
            'PRIMARY KEY(sitecontent_id, sitecontent_language, tag_id)',
        ], $tableOptions);

        // creates index for column `sitecontent_id`
        $this->createIndex(
            'idx-sitecontent_tag-sitecontent_id',
            '{{%sitecontent_junction_tags}}',
            'sitecontent_id'
        );

        // add foreign key for table `sitecontent`
        $this->addForeignKey(
            'fk-sitecontent_tag-sitecontent_id',
            '{{%sitecontent_junction_tags}}',
            'sitecontent_id, sitecontent_language',
            'sitecontent',
            'id, language',
            'CASCADE'
        );

        // creates index for column `tag_id`
        $this->createIndex(
            'idx-sitecontent_tag-tag_id',
            '{{%sitecontent_junction_tags}}',
            'tag_id'
        );

        // add foreign key for table `sitecontent_tags`
        $this->addForeignKey(
            'fk-sitecontent_tag-tag_id',
            '{{%sitecontent_junction_tags}}',
            'tag_id',
            'sitecontent_tags',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%sitecontent}}');
        $this->dropTable('{{%sitecontent_tags}}');
        $this->dropTable('{{%sitecontent_junction_categories}}');
        $this->dropTable('{{%sitecontent_junction_tags}}');
    }
}
