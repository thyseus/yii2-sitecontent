# Yii2-sitecontent

Small Sitecontent module for the Yii framework.

Can handle different types of sitecontent like:

* blogposts
* sitecontent
* emails

It uses the Summernote (http://summernote.org/) WYSIWYG Editor.

## Installation

```bash
$ composer require thyseus/yii2-sitecontent
$ php yii migrate/up --migrationPath=@vendor/thyseus/yii2-sitecontent/migrations
```

## Configuration

Add following lines to your main configuration file:

```php
'modules' => [
    'sitecontent' => [
        'class' => 'thyseus\sitecontent\Module',
        'modelClass' => '\app\models\User', // optional. your User model. Needs to be ActiveRecord.
    ],
],
```

By default only users that apply to $user->can('admin') are allowed to access the sitecontent administation.
You can modify this with the 'accessCallback' configuration option.

## Routes

Use the following routes to access the sitecontent module:

* index: https://your-domain/sitecontent/sitecontent/index
* view: https://your-domain/sitecontent/sitecontent/view?id=<slug>?lang=<lang>
* view: https://your-domain/sitecontent/sitecontent/view?id=<slug>

## License

Yii2-sitecontent is released under the GPLv3 License.
